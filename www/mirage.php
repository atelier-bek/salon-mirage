<div class="menu">
        <a href="#top">Salon Mirage</a>
        <a href="#editor" >Éditeurs / Publishers</a>
        <a href="#expo">Expositions / Exhibitions</a>
        <a href="#concert">Concerts</a>

</div>

<div class="contentInfo">
    <div id="colLeft">
      <br>
      <div class="Txt">
        <p>Mirage est un&nbsp;festival dédié à&nbsp;l’édition qui se&nbsp;tiendra les <span>15, 16 et&nbsp;17 septembre</span> à&nbsp;Bruxelles. Le&nbsp;festival combine un&nbsp;salon d'édition, trois expositions et&nbsp;des concerts tous les soirs.
        </p>
        <br>
        <p>Mirage is&nbsp;a&nbsp;comics and graphics festival to&nbsp;be held on&nbsp;<span>15, 16 and 17 september</span> at&nbsp;Brussels. The festival combine a&nbsp;fair with editors, three exhibitions and concerts every night.
        </p>
      </div>
    </div>
    <div class="Infos">
      <ul id="editor"><u>Éditeurs / Publishers</u><br><br>
        <h1>15 septembre <br> 18:00 ⤻ 23:00<br>16 et 17 septembre <br> 11:00 ⤻ 20:00</h1>
        <h2>La Senne<br>rue de la Senne 82, 1000 Bruxelles</h2>
        <h4>
         <li><a href="https://animalpress.tumblr.com/">Animal Press</a></li>
         <li><a href="http://lecontrevent.tumblr.com/" target="_blank">Antoine Orand</a></li>
         <li><a href="http://www.oficina-arara.org/" target="_blank">Arara</a></li>
         <li><a href="http://www.autobahnprint.be/" target="_blank">Autobahn</a></li>
         <li><a href="https://www.facebook.com/bandedeedition/?fref=mentions" target="_blank">Bande 2</a></li>
         <li><a href="http://www.bichel.be/" target="_blank">Bichel éditions</a></li>
         <li><a href="http://blocbooks.tumblr.com/" target="_blank">Bloc Books</a></li>
         <li><a href="http://www.collectionrevue.com/" target="_blank">Collection revue</a></li>
         <li><a href="http://www.copiedouble.org/" target="_blank">Copie Double</a></li>
         <li><a href="http://g----------a.tumblr.com/" target="_blank">CSH</a></li>
         <li><a href="http://cuistax-cuistax.blogspot.be/" target="_blank">Cuistax</a></li>
         <li><a href="http://danslecieltoutvabien.tumblr.com/" target="_blank">DLCTVB</a></li>
         <li>Editions Abstraites</li>
         <li><a href="http://editionsgitan.tumblr.com/" target="_blank">Éditions Gitan</a></li>
         <li><a href="http://editionspeinture.com/" target="_blank">Éditions Peinture</a></li>
         <li>Eliot Dadat</li>
         <li>Giga Coma Science Group</li>
         <li><a href="http://sugarlandparadise.tumblr.com/" target="_blank">Jeong Hwa Min</a></li>
         <li><a href="http://www.fremok.org/site.php?type=P&id=286" target="_blank">KnockOutsider komiks</a></li>
         <li>Lasse Wandschneider</li>
         <li><a href="https://linstitutserigraphique.tumblr.com/" target="_blank">L’institut Sérigraphique</a></li>
         <li><a href="https://www.matiere.org/" target="_blank">Matière</a></li>
         <li><a href="http://www.modelepuissance.com/" target="_blank">Modèle puissance</a></li>
         <li><a href="#">MULTI/Tabbert 4500</a></li>
         <li><a href="http://nkzine.free.fr/" target="_blank">Nazi Knife</a></li>
         <li><a href="http://novland.bigcartel.com/" target="_blank">Novland</a></li>
         <li>Numéro 10</li>
         <li><a href="http://papiermachine.fr" target="_blank">Papier machine</a></li>
         <li><a href="http://www.postindustrialanimism.net/" target="_blank">Post Industrial Animism</a></li>
         <li><a href="http://www.quentinchambry.com/" target="_blank">Quentin Chambry</a></li>
         <li><a href="http://www.romeojulien.fr/" target="_blank">Roméo Julien</a></li>
         <li><a href="https://www.facebook.com/rubierodareixira/" target="_blank">Rubiero da Reixirà</a></li>
         <li><a href="http://cargocollective.com/sammystein" target="_blank">Sammy Stein</a></li>
         <li><a href="https://www.shoboshobo.com/" target="_blank">Shoboshobo</a></li>
         <li><a href="http://stefanie-leinhos.de/" target="_blank">Stéphanie Leinhos</a></li>
         <li><a href="https://www.facebook.com/sprstrctr/?fref=ts" target="_blank">Super-Structure</a></li>
         <li><a href="http://surfaces-utiles.org/" target="_blank">Surfaces Utiles</a></li>
         <li>Tobboggan</li>
         <li><a href="http://jolidessin.blogspot.be/2013/08/tom-lebaron-kherif.html" target="_blank">Tom Lebaron Khérif</a></li>
         <li><a href="http://www.editions-tusitala.org/" target="_blank">Tusitala</a></li>
       </h4>
    </ul>
    <br><br>
     <ul id="expo"><u>Expositions / Exhibitions</u><br><br>
       <h1>
       Vernissage le 15 septembre à 18:00
      <br>15 septembre <br> 18:00 ⤻ 23:00
       <br>16 et 17 septembre <br> 11:00 ⤻ 20:00
       </h1>
       <h2>La Senne<br>rue de la Senne 82, 1000 Bruxelles</h2>
       <h4>
       <li>ARARA / retrospective<br>
          <em>Miguel Carneiro, Dayana Lucas, Bruno Borges, João Avves, Pedro Nora & Luís Silva.</em></li>
<br>        
<li>AND NO MORE FRIENDS<em><br> 
Alexander II,
<a href="http://cargocollective.com/antoineorand"  target="_blank" >Antoine Orand</a>,
<a href="https://www.instagram.com/haunted_horfee/" target="_blank" >Antwan Horfee</a>,
<a href="https://www.spraydaily.com/p/gues-pal-sdk-i-am-over-the-city/" target="_blank" >Guess</a>, 
<a href="http://nkzine.free.fr/" target="_blank" >Hendrik Hegray</a>,
Jérôme Benita,
<a href="http://www.dda-ra.org/fr/oeuvres/DELABORDE_Jonas" target="_blank" >Jonas Delaborde</a>,
<a href="http://www.leonsadler.com/" target="_blank" >Leon Sadler</a>,
<a href="https://www.instagram.com/picardomario/?hl=fr" target="_blank" >Mario Picardo</a>,
<a href=" https://www.instagram.com/sarahlouisebarbett/?hl=fr" target="_blank">Sarah Louise Barbett</a> & <a href="http://simon-john-thompson.com/" target="_blank" >Simon Thompson</a>.<br>
Commissariat de Anna Lejemmetel et Manuel Morin.</em></li><br>
        <li>PREMIER RENCARD <em><br><a href="http://www.quentinchambry.com/"target="_blank">Quentin Chambry</a>,
          <a href="https://www.flickr.com/photos/51641802@N02/"target="_blank">Alexis Poline</a>,
          <a href="https://www.instagram.com/hard_style_doodle/"target="_blank">Adrien Frégosi
          <a href="https://www.instagram.com/hard_style_doodle/"target="blank"> Gsulf</a>  </em></li>
      </h4>
      </ul>
    <br><br>
     <ul id="concert"><u>Concerts</u><br><br>
       <div class="Date1">
        <h1>15 Septembre <br> 20:00 ⤻  23:30</h1>
        <h2>La Senne<br>rue de la Senne 82, 1000 Bruxelles</h2>
        <h3>prix&thinsp;: 5€</h3>
        <h4>
          <li>
            <a href="" target="_blank">Z.B Aids </a>
          </li>
          <li>
            <a target="_blank">Pat Weld</a>
          </li>
          <li>
            <a target="_blank">Bear Bones Lay Low</a>
          </li>
          <li>
            <a target="_blank">dj set&thinsp;: Pavé de Bonnes Intentions</a>
          </li>
        </h4>
       </div>
<br>
<br>
       <div class="Date2">
         <h1>16 Septembre <br> 20:00 ⤻  23:30</h1>
         <h2>Hangar Communa<br>rue Gray 171, 1050 Ixelles</h2>
         <h3>prix&thinsp;: 5€</h3>
         <h4>
          <li>
            <a target="_blank">Baptiste Brunello</a>
          </li>
          <li>
            <a target="_blank">Dj Backstabber</a>
          </li>
          <li>
            <a target="_blank">The Choolers Division</a>
          </li>
         </h4>
       </div>
<br><br>
       <div class="Date3">
         <h1>17 Septembre <br> 20:00 ⤻ 23:30</h1>
         <h2>Niko Matcha<br>rue de Flandres 183B, 1000 Bruxelles</h2>
         <h3>prix&thinsp;: 5€</h3>
         <h4>
          <li>
            <a target="_blank">Accou</a>
          </li>
          <li>
            <a target="_blank">Capelo</a>
          </li>
           <li>
             <a target="_blank">Roméo Poirier</a>
           </li>
         </h4>
       </div>
      </ul>
      <!-- <img src="img/SOLEILNOIR.png" style="width: 15%; margin-top: 15px; opacity: 0.8;" alt="" /> -->
     </div>
   <div class="images">
     <img class="last img4" src="img/case_pyramidnoir.png" alt="">

   </div>
    <div class="credit">Design et développement&thinsp;: Luuse | typographie&thinsp;: <a href="http://lunchtype.com/" target="_blank" >Lunchtype</a></div>
</div>
