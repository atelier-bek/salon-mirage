<?php if($debug == true) ini_set('display_errors', 'On'); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <link rel="stylesheet" type="text/css" href="style/reset.css" />
    <link rel="stylesheet" type="text/css" href="style/main.css" />
    <link rel="stylesheet" media="(max-width: 800px)" href="style/mobile.css" />
    <!-- <link rel="stylesheet/less" type="text/css" href="style/main.less" /> -->
    <script type="text/javascript" src="js/lib/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/lib/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="js/lib/less.min.js"></script>
  </head>
  <body id="top" >
