#!/usr/bin/env bash

rm -f output/*.bmp
rm -f output/*.jpg
direct=$1
name=${direct%.ttf}
rm -rf output/$name

mkdir output/$name
mkdir output/$name/ascii

python Font2Img.py input/$1 $name

mogrify -format jpg output/$name/*.bmp
rm -f output/$name/*.bmp

for letter in output/$name/*.jpg
do
  nameLetter=${letter##*/}
  nameLetter=${nameLetter%.jpg}

  jp2a $letter --chars='/ ' --height=$2
  jp2a $letter --chars='/ ' --height=$2 > output/$name/ascii/$nameLetter.txt
done

rm ../ascii/*.txt
cp output/$name/ascii/*.txt ../ascii/



